import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class AccessoryForm extends StatefulWidget {
  /// The FocusNode of the fields who will use this accessory_view
  final List<FocusNode> focus;

  /// The action to be taken when pressed "Finished" on the last field
  final VoidCallback onSubmitted;

  /// The color of the helper texts
  final Color textColor;

  /// The string to represent "Next" on the form
  final String nextString;

  /// The string to represent "done" on the form
  final String doneString;

  //Form widgets
  final Key formKey;
  final Widget child;
  final bool autovalidate;
  final WillPopCallback onWillPop;
  final VoidCallback onChanged;

  const AccessoryForm({
    this.focus,
    this.formKey,
    @required this.child,
    this.textColor,
    this.nextString = 'Próximo', //Defaults to pt-BR
    this.doneString = 'Concluído', //Defaults to pt-BR
    this.autovalidate = false,
    this.onWillPop,
    this.onChanged,
    this.onSubmitted
  });

  @override
  AccessoryFormState createState() => AccessoryFormState();
}

class AccessoryFormState extends State<AccessoryForm> {

  int _listenerId;
  OverlayEntry _inputAccessoryViewOverlay;
  Color helpTextColor;

  @override
  void initState() {
    super.initState();

    helpTextColor = widget.textColor;

    if (Platform.isIOS) {
      this._listenerId = KeyboardVisibilityNotification().addNewListener(
          onShow: () {
            Future.delayed(Duration(milliseconds: 300), () {
              _showOverlay();
            });
          },
          onHide: () {
            _hideOverlay();
          }
      );
    }
  }

  void _showOverlay() {
    _inputAccessoryViewOverlay = InputAccessoryView.showOverlay(
        context: context,
        focus: widget.focus,
        onSubmitted: widget.onSubmitted,
        helpTextColor: widget.textColor,
        nextString: widget.nextString,
        doneString: widget.doneString
    );
  }

  void _hideOverlay() {
    if(_inputAccessoryViewOverlay != null) {
      _inputAccessoryViewOverlay.remove();
      _inputAccessoryViewOverlay = null;
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (Platform.isIOS) {
      _hideOverlay();
      KeyboardVisibilityNotification().removeListener(this._listenerId);
      KeyboardVisibilityNotification().dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: widget.formKey,
        child: widget.child,
        autovalidate: widget.autovalidate,
        onWillPop: widget.onWillPop,
        onChanged: widget.onChanged
    );
  }
}

class InputAccessoryView extends StatelessWidget {
  final List<FocusNode> focus;
  final VoidCallback onSubmitted;
  Color helpTextColor;
  String nextString;
  String doneString;

  InputAccessoryView({this.focus, this.onSubmitted, this.helpTextColor, this.nextString, this.doneString});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Divider(height: 1.0, color: helpTextColor,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(),
                  CupertinoButton(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      focus.where((f) => f.hasFocus).length == 0 ? '' : focus.last.hasFocus ? this.doneString : this.nextString,
                      style: TextStyle(color: helpTextColor, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      var focusedIndex = focus.indexWhere((focus) => focus.hasFocus);

                      if (focusedIndex != null) {
                        //Check if the focused is the last one
                        if (focusedIndex == focus.length - 1) {
                          //Finish form
                          if (onSubmitted != null) {
                            onSubmitted();
                          }
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                        else {
                          //Focus next
                          InputAccessoryView.nextFocus(context, focus[focusedIndex], focus[focusedIndex + 1]);
                        }
                      }
                    },
                  )
                ],
              ),
            )
          ],
        )
    );
  }

  static OverlayEntry showOverlay({BuildContext context, List<FocusNode> focus, VoidCallback onSubmitted, Color helpTextColor, String nextString, String doneString}) {
    var overlayEntry = OverlayEntry(builder: (context) {
      return Positioned(
        right: 0.0,
        bottom: MediaQuery.of(context).viewInsets.bottom,
        left: 0.0,
        child: InputAccessoryView(
          focus: focus,
          onSubmitted: onSubmitted,
          helpTextColor: helpTextColor,
          nextString: nextString,
          doneString: doneString,
        ),
      );
    });

    Overlay.of(context).insert(overlayEntry);

    return overlayEntry;
  }

  static void nextFocus(BuildContext context, FocusNode current, FocusNode next) {
    current.unfocus();
    FocusScope.of(context).requestFocus(next);
  }
}