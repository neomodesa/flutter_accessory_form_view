# flutter_accessory_form_view

A Form with an iOS'InputAccessoryView-like widget.

## Why?

Numpads on iOS does not have the next/done button, so the regular form navigation does not work. This widget provides a way to adopt the same solution on Flutter that is used on the native: to have a Next/Done button over the keyboard.

## Getting Started

Just replace the `Form` you already have with the `AccessoryForm`.

```dart
AccessoryForm(
	focus: [_nameFocus, _emailFocus],
	onSubmitted: () => _signupUser(),
	child: ListView(
		children: <Widget>[
			TextFormField(
				controller: _nameController,
				focus: _nameFocus,
			),
			TextFormField(
				controller: _emailController,
				focus: _emailFocus,
			),
		]
	),
)
```

##### Parameters
- `focus`: A list of all the focus which the AccessoryView will navigate through. Basically, the focus of all the fields on the form. The order **IS** relevant to identify when the form has been completed.
- `onSubmitted`: A `VoidCallback` to be called when the user has the last field selected and taps on "Done".
- `textColor`: The color of the helper texts;
- `nextString` & `doneString`: The strings to be shown on the widget;